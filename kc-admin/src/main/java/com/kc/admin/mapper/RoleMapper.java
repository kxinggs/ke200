package com.kc.admin.mapper;

import com.kc.admin.model.Role;
import org.apache.ibatis.annotations.Param;

import java.util.Set;

/**
 * author shish
 * Create Time 2019/1/13 19:37
 * author email shisheng@live.com
 * website www.bangnila.com
 **/

public interface RoleMapper {
    /**
     * 根据用户id查询角色信息
     * @param uid
     * @return
             */
    Set<Role> findRolesByUserId(@Param("uid") Integer uid);

    /**
     * 给admin用户添加 userInfo:del 权限
     * @param roleId
     * @param permissionId
     * @return
     */
    int addPermission(@Param("roleId") int roleId,@Param("permissionId") int permissionId);

    /**
     * 删除admin用户 userInfo:del 权限
     * @param roleId
     * @param permissionId
     * @return
     */
    int delPermission(@Param("roleId") int roleId,@Param("permissionId") int permissionId);
}
