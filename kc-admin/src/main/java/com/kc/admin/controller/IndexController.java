package com.kc.admin.controller;

import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * author shish
 * Create Time 2019/1/12 14:24
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@Controller
public class IndexController {
    @RequestMapping("/notLogin")
    public  String  login(){
        return "login";
    }


    @RequestMapping("/index")
    public  String  index(){
        return "index";
    }


    @RequestMapping("/welcome.html")
    public  String  welcome(){
        return "welcome";
    }


    @RequestMapping("/admin-add.html")
    public  String  admin_add(){
        return "admin-add";
    }


    @RequestMapping("/admin-cate.html")
    public  String  admin_cate(){
        return "admin-cate";
    }


    @RequestMapping("/admin-edit.html")
    public  String  admin_edit(){
        return "admin-edit";
    }

    @RequestMapping("/admin-list.html")
    public  String admin_list(){
        return "admin-list";
    }


    @RequestMapping("/admin-role.html")
    public  String admin_role(){
        return "admin-role";
    }

    @RequestMapping("/admin-rule.html")
    public  String admin_rule(){
        return "admin-rule";
    }

    //打卡管理
    @RequestMapping("/order-list.html")
    public  String   order_list(){
        return "order-list";
    }


    @RequestMapping("/delCookie")
    public String logout(HttpServletRequest request) {
        SecurityUtils.getSubject().logout(); // session删除、RememberMe cookie
        // 也将被删除
        return "login";
    }

//    @RequestMapping("/notRole")
//    public String notRole() {
//        return "404";
//    }
//    @RequestMapping("/favicon.ico")
//   public  String  favicon(){
//        return "static/favicon.ico";
//   }
}
