package com.mybatisplus.kc.service.lmp;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mybatisplus.kc.mapper.DeptMapper;
import com.mybatisplus.kc.model.DeptEntity;
import com.mybatisplus.kc.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * author shish
 * Create Time 2019/3/8 15:25
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@Service
public class DeptServicelmp implements DeptService {
   @Autowired
    private DeptMapper deptMapper;

    @Override
    public int addDept(DeptEntity deptEntity) {
         return deptMapper.insert(deptEntity);
        //return deptMapper.insertDept(deptEntity);
    }

    @Override
    public int delDept(ArrayList ids) {
        return deptMapper.deleteBatchIds(ids);
    }

    @Override
    public int delDept(Integer id) {
        return deptMapper.deleteById(id);
    }

    @Override
    public int update(DeptEntity deptEntity) {
        return deptMapper.updateById(deptEntity);
    }

    @Override
    public DeptEntity selectOne(Integer id) {
        return deptMapper.selectById(id);
    }

    @Override
    public List<DeptEntity> selectDeptList(Integer uid) {
        QueryWrapper<DeptEntity> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("uid",uid);
        return deptMapper.selectList(queryWrapper);
    }
}

