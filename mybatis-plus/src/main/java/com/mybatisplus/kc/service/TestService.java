package com.mybatisplus.kc.service;

import com.mybatisplus.kc.model.MenuEntity;

/**
 * author shish
 * Create Time 2019/3/7 16:16
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public interface TestService {
    int test(MenuEntity menuEntity);
}
