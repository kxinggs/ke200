package com.mybatisplus.kc.service.lmp;

import com.mybatisplus.kc.mapper.TestMapper;
import com.mybatisplus.kc.model.MenuEntity;
import com.mybatisplus.kc.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * author shish
 * Create Time 2019/3/7 16:16
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@Service
public class TestServicelmp implements TestService {
    @Autowired
    private TestMapper testMapper;
    @Override
    public int test(MenuEntity menuEntity) {
        return testMapper.insert( menuEntity);
    }
}
