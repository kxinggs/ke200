package com.mybatisplus.kc.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * author shish
 * Create Time 2019/1/13 19:34
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@TableName("user_info")
public class User implements Serializable {

    /**
     * 用户id(主键 自增)
     */
    @TableId(value="uid",type= IdType.AUTO)
    private Integer uid;

    /**
     * 用户名
     */
    @TableField(exist = true)
    private String username;

    /**
     * 登录密码
     */
    @TableField(exist = true)
    private String password;

    /**
     * 用户真实姓名
     */
    @TableField(exist = true)
    private String name;

    /**
     * 身份证号
     */
    @TableField(exist = true)
    private String id_card_num;

    /**
     * 用户状态：0:正常状态,1：用户被锁定
     */
    @TableField(exist = true)
    private String state;

    @TableField(exist = true)
    private String nickname;//昵称
    @TableField(exist = true)
    private  String real_name;//真实名字
    @TableField(exist = true)
    private  String tel;//手机号
    @TableField(exist = true)
    private  String email;//电子邮箱
    @TableField(exist = true)
    private  String per_sig;//个性签名
    @TableField(exist = true)
    private  String per_url;//头像
    @TableField(exist = true)
    private String salt;
    @TableField(exist = true)
    private  String question;//找回密码问题
    @TableField(exist = true)
    private  String answer;//答案
    @TableField(exist = true)
    private Date register_time;//注册的时间
    @TableField(exist = true)
    private  Date update_time; //密码修改日期


    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getReal_name() {
        return real_name;
    }

    public void setReal_name(String real_name) {
        this.real_name = real_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPer_sig() {
        return per_sig;
    }

    public void setPer_sig(String per_sig) {
        this.per_sig = per_sig;
    }

    public String getPer_url() {
        return per_url;
    }

    public void setPer_url(String per_url) {
        this.per_url = per_url;
    }

    public Date getRegister_time() {
        return register_time;
    }

    public void setRegister_time(Date register_time) {
        this.register_time = register_time;
    }

    public Date getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(Date update_time) {
        this.update_time = update_time;
    }

    /**
     * 用户所拥有的所有角色
     */
    @TableField(exist = false)
    private Set<Role> roles = new HashSet<>();

    @Override
    public String toString() {
        return "User{" +
                "uid=" + uid +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", id_card_num='" + id_card_num + '\'' +
                ", state='" + state + '\'' +
                ", roles=" + roles +
                '}';
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId_card_num() {
        return id_card_num;
    }

    public void setId_card_num(String id_card_num) {
        this.id_card_num = id_card_num;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}
