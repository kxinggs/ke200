var selectionrow
var selectLength

function add0(m){return m<10?'0'+m:m }
function format(shijianchuo)
{
  //shijianchuo是整数，否则要parseInt转换
    var time = new Date(parseInt(shijianchuo));
    console.log(time)
    var y = time.getFullYear();
    var m = time.getMonth()+1;
    var d = time.getDate();
    var h = time.getHours();
    var mm = time.getMinutes();
    var s = time.getSeconds();
    return y+'-'+add0(m)+'-'+add0(d)+' '+add0(h)+':'+add0(mm)+':'+add0(s);
}
var vm=new Vue({
    el: '#app',
    data: {
        visible: false,
        mapShow:false,
        title:"",
         sign:{
             week_num:"",
             lou:"",
             start_time:"",
             longitude:"",
             latitude:"",
             course_id:"",
             class_grade:"",
             end_time:'',
             distance:""
         },
        ruleValidate:{
            week_num:[

            ],
            lou:[

            ],
            course_id:[

            ],
            map:[

            ],
            start_time:[

            ]
        },
        columns4: [
            {
                type: 'selection',
                width: 60,
                align: 'center'
            },
            {
                title: '打卡周次',
                key: 'week_num'
            },
            {
                title: '楼号',
                key: 'lou'
            },
            {
                title: '签到时间',
                key: 'start_time',
                // render:function (h, params) {
                //     const row = params.row;
                //     console.log(row.start_time)
                //     return h('div',format(row.start_time));/*这里的this.row能够获取当前行的数据*/
                // }
            },
            // {
            //     title: '签到地图',
            //     key: 'map'
            // },
            {
                title: '课程名称',
                key: 'course_name'
            },
            {
                title: '班级号',
                key: 'class_grade'
            }
        ],
        listData:[],
        course:"",
        course_id:"",
        alert:""
    },
    mounted:function(){
        this.getList()
    },
    methods: {

        show: function () {
            this.visible = true;
        },
        add:function () {
            vm.title="增加签到"
             vm.sign={
                  id:"",
                 week_num:"",
                 lou:"",
                 start_time:"",
                 longitude:"",
                 latitude:"",
                 course_id:"",
                 class_grade:"",
                 end_time:"",
                 distance:""
             }
            this.getCourseList()
            this.visible = true;
        },
        update:function () {
            vm.title="修改签到"
         if (selectLength!=1){
             alert("只能选择一条")
         }
         var id=  selectionrow.id
            vm.visible=true
            console.log(id)
            this.getInfo(id)
            this.getCourseList()
        },
        del:function () {
            var id=  selectionrow.id
            console.log(id)
            axios({
                method: 'post',
                url:"../class//del?"+"id="+id,
            }).then(function(res){
                vm.reload()
                console.log(res.data)
            })
        },
        sign_map:function(){
            this.sign.longitude =$("#lon").val();
            this.sign.latitude=$("#lat").val();
            this.alert="您已选择了考勤地址"
            this.mapShow=false
        },
        updateOrSave :function(){
            var lang=$("#lon").val();
            var lat=$("#lat").val();
            console.log("经度"+lang+"维度"+lat)
            var id=vm.sign.id
              console.log(id.length)
              console.log(id)
            vm.sign.course_id=vm.course_id
            // console.log("添加的课程"+vm.course_id)
            var len=id.length
            var st_time=vm.sign.start_time;
            var en_time=vm.sign.end_time
            console.log(st_time+en_time)
            if (st_time>=en_time){
                alert("最晚签到时间不能小于最早签到时间，请将时间设置为大于十分钟以上")
                return
            }
            if (len==0){
                console.log("确认增加")
                console.log("时间"+vm.sign.start_time)
                axios({
                    method:'post',
                    url:"../class/add",
                    contentType: 'application/json;charset=utf-8',
                    params:vm.sign
                }).then(function(res){
                     vm.reload()
                })
            }else {
                console.log("确认修改")
                axios({
                    method:'post',
                    url:"../class/update",
                    contentType: 'application/json;charset=utf-8',
                    params:vm.sign
                }).then(function(res){
                    vm.reload()
                })
            }

        },
        changeDate:function(date){
               vm.sign.start_time=date
                console.log(date)
        },
        changeDate_1:function(date){
            vm.sign.end_time=date
            console.log(date)
        },
         saveInfo:function(variable){
             axios({
                 method: 'post',
                 url:"../class/add",
             }).then(function(res){
                 vm.reload()
             console.log(res.data)
         })
         },
        updateInfo:function(variable){
            axios({
                method: 'post',
                url:"../class/update",
            }).then(function(res){
                vm.reload()
                vm.getList()
            console.log(res.data)
        })
        },

        getList:function () {
            console.log("请求数据！！！")
            axios({
                method: 'post',
                url:"../class/list",
            }).then(function(res){
                vm.listData=res.data.list
                console.log(res.data)
        })
        },
        getInfo:function(id){
            axios({
                method: 'post',
                url:"../class/obj?"+"id="+id,
            }).then(function(res){
                vm.sign=res.data.obj
                vm.sign.start_time=format(res.data.obj.start_time)
                console.log(res.data)
               this.visible = true;
            })
        },
        getCourseList:function(){
            axios({
                url:"../teacher/select_list",
            }).then(function(res){
                console.log("获取签到的课程")
               vm.course=res.data.list
                console.log(res)
            })
        },
        getMap:function(){
            console.log("打卡地图")
            this.mapShow=true
        },
        isSelect:function (selection, row) {
            console.log("选中"+selection.length)
            selectLength=selection.length
            selectionrow=row
            selectionrow
        },
        NoSelect:function (selection, row) {
            console.log("取消"+selection.length)
            selectLength=selection.length
            selectionrow=null;
        },
        all_ok:function (selection) {
            selectLength=0
            console.log("all选中"+selection)
            selectionrow=null;
        },
        all_cancel:function (selection) {
            selectLength=0
            console.log("all取消"+selection)
            selectionrow=null;
        },
        change_status:function(){
            console.log("状态"+vm.course_id)
        },
        reload:function () {
            //页面刷新，目前用的location
            location.reload()
        }
    }
})