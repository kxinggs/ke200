
var selectionrow
var selectLength
var vm=new Vue({
    el: '#app',
    data: {
        visible: false,
        upModel:false,
        ruleValidate:{
            name:[],
            num:[],
            grade:[],
            class:[]
        },
        dept:{
            id:"",
            dept_name:"",
            dept_num:"",
            dept_grade:"",
            dept_class:"",
            uid: ""
        },
        columns4: [
            {
                type: 'selection',
                width: 60,
                align: 'center'
            },
            {
                title: 'id',
                key: 'id'
            },
            {
                title: '学院名字',
                key: 'dept_name'
            },
            {
                title: '学院编号',
                key: 'dept_num'
            },
            {
                title: '学院年级',
                key: 'dept_grade'
            },
            {
                title: '班级',
                key: 'dept_class'
            }
        ],
        dept_list:[],
        dept_temp:{
         dept_name:"",
            dept_num:"",
         dept_class:"",
         dept_grade:""
        }
    },
    mounted:function(){
        this.getList()
    },
    methods: {
        show: function () {
            this.visible = true;
        },
        add:function () {
            this.visible = true
        },
        update:function () {
            console.log(selectionrow)
            if (selectionrow==null&&!selectLength.valueOf(1)){
                alert("请选中以下一条数据")
                return
            }
            var id=selectionrow.id
            console.log(id)
            vm.upModel=true
            axios({
                method: 'post',
                url:"../dept/select_one?"+"id="+id,
            }).then(function(res){
                console.log(res.data.rerult)
                vm.dept=res.data.rerult
            })
            console.log("获得的数据"+JSON.stringify(vm.dept))
        },
        del:function () {
            var id=selectionrow.id
           axios({
               method: 'post',
               url:"../dept/del?"+"id="+id,
           }).then(function(res){
               console.log(res.data)
            vm.reload()
           })
        },
        getList:function () {
            console.log("请求数据！！！")
            axios({
                method: 'post',
                url:"../dept/select_list",
            }).then(function(res){
                console.log(res.data)
                vm.dept_list=res.data.list
            })
        },


        saveInfo:function(){
         console.log("保存")
            console.log(vm.dept)
            vm.dept_temp.dept_class=vm.dept.class
            vm.dept_temp.dept_grade=vm.dept.grade
            vm.dept_temp.dept_num=vm.dept.num
            vm.dept_temp.dept_name=vm.dept.dept_name
            var  dept_class;
            var dept_grade;
            var dept_num;
            var dept_name;
           // $.ajax({
           //     url:"../dept/add?"+"dept_name"+"="+vm.dept.dept_name+"&"+"dept_num"+"="+vm.dept.num+"&"+"dept_grade"+"="+vm.dept.grade+"&"+"dept_class"+"="+vm.dept.class,
           //     dataType: "json",
           //     success:function (r) {
           //       console.log("打印")
           //         console.log(r)
           //     }
           // })
            axios({
                  method: 'post',
                   url:"../dept/add?"+"dept_name"+"="+vm.dept.dept_name+"&"+"dept_num"+"="+vm.dept.num+"&"+"dept_grade"+"="+vm.dept.grade+"&"+"dept_class"+"="+vm.dept.class,
            }).then(function(res){
                vm.reload()
            })
        },
        updateInfo:function(){
            axios({
                method: 'post',
                url:"../dept/update?"+"id="+vm.dept.id+"&"+"dept_name"+"="+vm.dept.dept_name+"&"+"dept_num"+"="+vm.dept.dept_num+"&"+"dept_grade"+"="+vm.dept.dept_grade+"&"+"dept_class"+"="+vm.dept.dept_class,
            }).then(function(res){
                vm.reload()
        })
        },
        isSelect:function (selection, row) {
            console.log("选中"+selection.length)
             selectLength=selection.length
             selectionrow=row
             selectionrow
        },
        NoSelect:function (selection, row) {
            console.log("取消"+selection.length)
            selectLength=selection.length
            selectionrow=null;
        },
        all_ok:function (selection) {
            selectLength=0
            console.log("all选中"+selection)
            selectionrow=null;
        },
        all_cancel:function (selection) {
            selectLength=0
            console.log("all取消"+selection)
            selectionrow=null;
        },
        reload:function () {
            //页面刷新，目前用的location
            location.reload()
        }
    }
})
