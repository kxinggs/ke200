package com.kc.platform.config;

import com.kc.platform.interceptor.AuthorizationInterceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * author shish
 * Create Time 2019/1/11 17:11
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@Configuration
public class AuthorizationInterceptorConfig  extends WebMvcConfigurerAdapter {
   @Autowired
    AuthorizationInterceptor authorizationInterceptor;

    public void  addInterceptors(InterceptorRegistry registry){
        InterceptorRegistration addInterceptor = registry.addInterceptor(authorizationInterceptor);
        registry.addInterceptor(authorizationInterceptor);
        //排除不拦截
        addInterceptor.excludePathPatterns("/api/login");

        // 拦截配置
        addInterceptor.addPathPatterns("/api/**");
    }
}
