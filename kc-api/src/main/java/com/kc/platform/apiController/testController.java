package com.kc.platform.apiController;

import com.kc.ke.common.utils.UploadUtils;
import com.kc.platform.Mapper.UserMapper;
import com.kc.platform.annotation.IgnoreAuth;
import com.kc.platform.annotation.LoginUser;
import com.kc.platform.common.R;
import com.kc.platform.model.User;
import com.kc.platform.service.ApiUserService;
import com.kc.platform.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * author shish
 * Create Time 2019/1/11 15:25
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@RestController
@RequestMapping("/api")
public class testController {
    @Autowired
    private ApiUserService userService;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private TokenService tokenService;
    @RequestMapping("/test")
    public R testController(@LoginUser User user){
        List list = (List) userMapper.gstListById(1);
        Map<String,Object> map= new HashMap<>();
        map.put("list",list);
        return R.ok(map);
    }
    @IgnoreAuth
    @RequestMapping("/login")
    public R login(Long number,String password,String openid){
        Map<String,Object> result=new HashMap<>();
        System.out.println(openid);
        User user=userService.queryObject(number);
        if (user.getOpenid().equals(openid)){
            //用户登录
            long userId = userService.login(number, password);

            if (userId==0){
                result.put("erro",0);
                return R.ok(result);
            }
            //生成token
            Map<String, Object> map = tokenService.createToken(userId);
            UploadUtils uploadUtils=new UploadUtils();
            String name=uploadUtils.upload();
            System.out.println(name);
            map.put("erro",1);
            return R.ok(map);
        }else {
            result.put("erro",2);
            return R.ok(result);
        }
    }
   @RequestMapping("/logoff")
   @IgnoreAuth
    public R logoff(String token){
        return null;
    }
}
