package com.kc.platform.service.lmp;

import com.kc.platform.Mapper.UserMapper;
import com.kc.platform.model.ClassTimeEntity;
import com.kc.platform.model.SignEntity;
import com.kc.platform.model.User;
import com.kc.platform.service.ApiUserService;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * author shish
 * Create Time 2019/1/11 16:14
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@Service
public class ApiUserServicelmp implements ApiUserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public User queryObject(@Param("userId") Long  userId) {
        User user=userMapper.queryObject(userId);
        return user;
    }

    @Override
    public long login(Long number, String password) {
            User user=userMapper.queryObject(number);
            Long userId=user.getUserId();
            if (DigestUtils.sha256Hex(password).equals(user.getPassword())){
                return userId;
            }else {
                return 0;
            }
    }

    @Override
    public List<ClassTimeEntity> QuerySignList(String grade, String time,String endtime) {
        return userMapper.QuerySignList(grade,time,endtime);
    }

    @Override
    public Integer save(User user) {
        user.setPassword(DigestUtils.sha256Hex(user.getPassword()));
        return userMapper.save(user);
    }

    @Override
    public User getUserByOpenId(String openid) {
        return userMapper.getUserByOpenId(openid);
    }

    public static void main(String[] args){
        System.out.println(DigestUtils.sha256Hex("123456"));
    }
}
