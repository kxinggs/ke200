package com.kc.platform.annotation;

import java.lang.annotation.*;

/**
 * author shish
 * Create Time 2019/1/11 15:29
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface IgnoreAuth {

}
