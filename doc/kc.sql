/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50722
Source Host           : localhost:3306
Source Database       : kc

Target Server Type    : MYSQL
Target Server Version : 50722
File Encoding         : 65001

Date: 2019-03-07 11:06:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ke_token
-- ----------------------------
DROP TABLE IF EXISTS `ke_token`;
CREATE TABLE `ke_token` (
  `user_id` bigint(20) NOT NULL,
  `token` varchar(255) NOT NULL,
  `expire_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ke_token
-- ----------------------------
INSERT INTO `ke_token` VALUES ('15112111', 'npcovw37rg7miidqo5aql3e4ybgrx770', '2019-02-04 01:21:14', '2019-02-03 13:21:14');

-- ----------------------------
-- Table structure for ke_user
-- ----------------------------
DROP TABLE IF EXISTS `ke_user`;
CREATE TABLE `ke_user` (
  `user_id` int(11) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `grade` varchar(255) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `sex` int(1) DEFAULT NULL,
  `nickname` varchar(10) DEFAULT NULL,
  `per_sign` varchar(100) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `question` varchar(255) DEFAULT NULL,
  `answer` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ke_user
-- ----------------------------
INSERT INTO `ke_user` VALUES ('1511211', '123456', '石胜', '大四', null, null, null, null, null, null, null, null);
INSERT INTO `ke_user` VALUES ('1511212', '123456', 'ad', '12', null, null, null, null, null, null, null, null);
INSERT INTO `ke_user` VALUES ('15112111', '123456', 'asa', 'as', null, null, null, null, null, null, null, null);
INSERT INTO `ke_user` VALUES ('15112133', '123456', '石大爷', '大一', null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission` (
  `menu_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '父编号列表',
  `parent_id` int(11) DEFAULT NULL COMMENT '父编号,本权限可能是该父编号权限的子权限',
  `name` varchar(50) DEFAULT NULL COMMENT '权限名称',
  `permission` varchar(100) DEFAULT NULL COMMENT '权限字符串,menu例子：role:*，button例子：role:create,role:update,role:delete,role:view',
  `resource_type` varchar(20) DEFAULT NULL COMMENT '资源类型，[menu|button]',
  `url` varchar(200) DEFAULT NULL COMMENT '资源路径 如：/userinfo/list',
  `available` char(1) DEFAULT '0' COMMENT '是否可用0可用  1不可用',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
INSERT INTO `sys_permission` VALUES ('1', '0', '用户管理', 'userInfo:view', 'menu', 'userInfo/view', '0');
INSERT INTO `sys_permission` VALUES ('2', '1', '用户添加', 'userInfo:add', 'button', 'userInfo/add', '0');
INSERT INTO `sys_permission` VALUES ('3', '1', '用户删除', 'userInfo:del', 'button', 'userInfo/del', '0');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `available` char(1) DEFAULT '0' COMMENT '是否可用0可用  1不可用',
  `role` varchar(20) DEFAULT NULL COMMENT '角色标识程序中判断使用,如"admin"',
  `description` varchar(100) DEFAULT NULL COMMENT '角色描述,UI界面显示使用',
  PRIMARY KEY (`id`),
  UNIQUE KEY `role` (`role`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '0', 'admin', '管理员');
INSERT INTO `sys_role` VALUES ('2', '0', 'vip', 'VIP会员');
INSERT INTO `sys_role` VALUES ('3', '1', 'test', '测试');

-- ----------------------------
-- Table structure for sys_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission` (
  `role_id` int(11) DEFAULT NULL COMMENT '角色id',
  `permission_id` int(11) DEFAULT NULL COMMENT '权限id',
  KEY `role_id` (`role_id`) USING BTREE,
  KEY `permission_id` (`permission_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role_permission
-- ----------------------------
INSERT INTO `sys_role_permission` VALUES ('1', '1');
INSERT INTO `sys_role_permission` VALUES ('1', '2');
INSERT INTO `sys_role_permission` VALUES ('2', '1');
INSERT INTO `sys_role_permission` VALUES ('2', '2');
INSERT INTO `sys_role_permission` VALUES ('2', '3');
INSERT INTO `sys_role_permission` VALUES ('1', '2');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `uid` int(11) DEFAULT NULL COMMENT '用户id',
  `role_id` int(11) DEFAULT NULL COMMENT '角色id',
  KEY `uid` (`uid`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1');
INSERT INTO `sys_user_role` VALUES ('2', '2');
INSERT INTO `sys_user_role` VALUES ('50', '1');

-- ----------------------------
-- Table structure for test
-- ----------------------------
DROP TABLE IF EXISTS `test`;
CREATE TABLE `test` (
  `id` int(11) NOT NULL,
  `zhou` varchar(255) DEFAULT NULL,
  `week` varchar(255) DEFAULT NULL,
  `jieci` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of test
-- ----------------------------
INSERT INTO `test` VALUES ('1', '1,2,3,4,5,6,7', '2,3,4,5', '1,2');

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT '' COMMENT '用户名',
  `password` varchar(256) DEFAULT NULL COMMENT '登录密码',
  `name` varchar(256) DEFAULT NULL COMMENT '用户名字(匿名)',
  `tel` varchar(255) DEFAULT NULL,
  `id_card_num` varchar(256) DEFAULT NULL COMMENT '用户身份证号',
  `state` char(1) DEFAULT '0' COMMENT '用户状态：0:正常状态,1：用户被锁定',
  `nickname` varchar(20) DEFAULT NULL COMMENT '昵称',
  `real_name` varchar(20) DEFAULT NULL COMMENT '实名',
  `email` varchar(20) DEFAULT NULL COMMENT '邮箱',
  `per_sig` varchar(200) DEFAULT NULL COMMENT '个性签名',
  `per_url` varchar(255) DEFAULT NULL COMMENT '头像地址',
  `salt` varchar(255) DEFAULT NULL COMMENT '加盐',
  `question` varchar(255) DEFAULT NULL,
  `answer` varchar(255) DEFAULT NULL,
  `register_time` datetime DEFAULT NULL COMMENT '注册事件',
  `update_time` datetime DEFAULT NULL COMMENT '密码修改时间',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`username`) USING BTREE,
  UNIQUE KEY `id_card_num` (`id_card_num`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_info
-- ----------------------------
INSERT INTO `user_info` VALUES ('1', 'admin', '$2a$10$VwLFpjvIRGpAAaV8pVfEHefHJ0po9R7ABUZ.3mhz3v1MyabkGRGUG', '超哥', null, '133333333333333333', '0', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `user_info` VALUES ('2', 'test', '$2a$10$VwLFpjvIRGpAAaV8pVfEHefHJ0po9R7ABUZ.3mhz3v1MyabkGRGUG', '孙悟空', null, '155555555555555555', '0', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `user_info` VALUES ('48', 'test1', '$2a$10$VwLFpjvIRGpAAaV8pVfEHefHJ0po9R7ABUZ.3mhz3v1MyabkGRGUG', null, null, null, '0', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `user_info` VALUES ('49', '10086', '$2a$10$9wZ29SJRBBOJ3CTztfvFYudBW/Fk/i0QfmOoAtMH9mugA/Q2aHrdu', null, null, null, '0', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `user_info` VALUES ('50', 'admin100', '$2a$10$zAwSx2EC7BJpwgioNXjLke6fODSonL8hxEwrOEBLhnh1AIp3oGbGG', null, null, null, '0', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `user_info` VALUES ('51', 'admin2121', '$2a$10$4.pjlvLM7MbAHGXwTgV.4OM8713BKKiXx.zOk//e9eOB0gb0KUFAO', null, null, null, '0', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `user_info` VALUES ('52', 'admin111', '$2a$10$gDeit9nglWPIBnqfOtDNF..6WIDvUYX920uRQBlTWkvRnNC7YJSCS', null, null, null, '0', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `user_info` VALUES ('53', 'admin12345', '$2a$10$e5U7I/bxRcpIK4xFR5zhwO7J3a.HSvAgROW4gD8Ub9mcl5g4J8Axy', null, null, null, '0', null, null, null, null, null, null, null, null, null, null);
