//从服务器端获取需要打卡的定位地址
function getLocationFromServer(){
  wx.request({
    url: '',
  })
}
//向服务端发送打卡地址
function postLocation(){
  wx.request({
    url: '',
  })
}
//计算是否在打卡的范围之内
function GetDistance(lat1, lng1, lat2, lng2) {

  var radLat1 = Rad(lat1);
  var radLat2 = Rad(lat2);
  var a = radLat1 - radLat2;
  var b = Rad(lng1) - Rad(lng2);
  var s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) +
    Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2)));
  s = s * 6378.137;// EARTH_RADIUS;
  s = Math.round(s * 10000) / 10000; //输出为公里
  //s=s.toFixed(4);
  return s;
}